const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: '192.168.5.33',
    password: 'mysecretpassword',
    database: 'cadem',
    port: 5432,
})

async function saveCandidato(candidato){
    const values = Object.values(candidato);
    const consulta = {
        text:
        "INSERT INTO candidatos (nombre, foto, color, votos) VALUES ($1, $2, $3, 0)",
        values,
    };
    try{
        const result = await pool.query(consulta);
        return result;
    }catch (e){
        return e;
    }
}

async function getCandidatos(){
    try{
        const result = await pool.query(`SELECT * FROM candidatos`);
        return result.rows;
    }catch(e){
        return e;
    }
}

async function getHistorial(){
    try{
        const consulta = {
            text: `SELECT * FROM historial`,
            rowMode: "array",
        };
    const result = await pool.query(consulta);
    return result.rows;
    }catch(e){
        return e;
    }
}

async function deleteCandidato(id){
    try{
       const result = await pool.query(`DELETE FROM candidatos WHERE id = ${id}`);
       return result.rows; 
    }catch(e){
        console.log(e);
        return (e);
    }
}

async function editCandidato(candidato){
    const values = Object.values(candidato);
    const consulta = {
        text: "UPDATE candidatos SET nombre = $1, foto = $2, WHERE id = $3 RETURNING *",
        values,
    };
    try{
        const result = await pool.query(consulta);
        return result.rows;
    }catch(e){
        return (e);
    }
}

async function saveVotos(voto){
    const values = Object.values(voto);
    const votoHistorial = {
        text: "INSERT INTO historial (region, votos, ganador) VALUES ($1, $2, $3)",
        values,
    }
    const votoCandidato = {
        text: "UPDATE candidatos SET votos = votos + $1 WHERE nombre = $2",
        values: [Number(values[1]), values[2]]
    };
    try{
        await pool.query("BEGIN");
        await pool.query(votoHistorial);
        await pool.query(votoCandidato);
        await pool.query("COMMIT");

        return true;

    }catch(e){
        await pool.query("ROLLBACK");
        console.log(e);
        return false;
    }
}

module.exports = {
    saveCandidato,
    getCandidatos,
    getHistorial,
    deleteCandidato,
    editCandidato,
    saveVotos,
};