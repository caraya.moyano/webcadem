const http = require('http');
const url = require('url');
const {
    saveCandidato,
    getCandidatos,
    getHistorial,
    deleteCandidato,
    editCandidato,
    saveVotos,
} = require("./bbdd/consultas");
const fs = require('fs');

http
.createServer((req, res)=>{
    
    //proceso para agregar un candidato (mirar en archivo consulta.js metodo saveCandidato)
    if(req.url == "/candidato" && req.method == "POST"){
        let body = "";
        console.log('inicia post')
        req.on("data", (chunk) =>{
            console.log(chunk.toString());
            body += chunk.toString();
        });
        req.on("end", async () => {
            console.log('body: ' + body);
            const candidato = JSON.parse(body);
            const result = await saveCandidato(candidato);
            console.log('body: ' + body);
            //console.log('Insert: ' + JSON.stringify(result));
            res.end(JSON.stringify(result));
        });
    }

    //proceso para traer todos los candidatos (mirar en archivo consulta.js metodo getCandidato)
    if(req.url == "/candidatos" && req.method == "GET"){
        (async () => {
            const candidatos = await getCandidatos();
            console.log('Candidatos: ' + JSON.stringify(candidatos));
            res.end(JSON.stringify(candidatos))
        })();
    }

    //proceso para eliminar un candidato (mirar en archivo consulta.js metodo deleteCandidato)
    if(req.url == "/candidato?id" && req.method == "DELETE"){
        (async () => {
            let { id } = url.parse(req.url, true).query;
            await deleteCandidato(id);
            res.end("Candidato Eliminado");
        })();
    }

    //proceso para actualizar un candidato (mirar en archivo consulta.js metodo editCandidato)
    if(req.url == "/candidato" && req.method == "PUT"){
        let body = "";
        req.on("data", (chunk)=>{
            body += chunk.toString();
        });
        req.on("end", async () => {
            const candidato = JSON.parse(body);
            console.log(candidato);
            const result = await editCandidato(candidato);
            res.end(JSON.stringify(result));
        });
    }

    //proceso para registrar votos(mirar en archivo consulta.js metodo saveVotos)
    if (req.url == "/votos" && req.method == "POST"){
        let body = "";
        req.on("data", (chunk) => {
            body += chunk.toString();
        });
        req.on("end", async () => {
            const voto = JSON.parse(body);
            const result = await saveVotos(voto);
            if(result){
                res.statusCode = 201;
                res.end(JSON.stringify(result));
            } else {
                res.statusCode = 500;
                res.end("");
            }
        })
    }

    //proceso para traer el historial (mirar en archivo consulta.js metodo getHistorial)
    if(req.url == "/historial" && req.method == "GET"){
        (async () =>{
            const historial = await getHistorial();
            res.end(JSON.stringify(historial));
        })();
    }

    //Entregamos el html como respuesta al llegar a / de nuestro sitio
    if (req.url == "/" && req.method == "GET"){
        fs.readFile("index.html", (err, data) => {
            res.setHeader("Content-Type","text/html");
            res.end(data);
        });
    }
})
.listen(3000)