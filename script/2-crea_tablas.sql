CREATE TABLE candidatos(
    id SERIAL,
    nombre VARCHAR(75),
    foto VARCHAR(200),
    color VARCHAR(20),
    votos INT
);

CREATE TABLE historial(
    region VARCHAR(100) UNIQUE,
    votos INT,
    ganador VARCHAR(75)
);